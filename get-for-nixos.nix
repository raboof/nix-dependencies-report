{ pkgs ? import <nixpkgs> {}
, nixos ? import "${<nixpkgs>}/nixos" {}
}:

let
  runtimeReport = pkgs.callPackage ./. {};
in
  (runtimeReport (nixos.system))
