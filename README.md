# nix-dependencies-report

Get a report of the runtime dependencies of a derivation

## Usage

For example, to get a report of hello:

```
$ cat $(nix-build get-for-hello.nix)
```

Or for nixos:

```
$ cat $(nix-build get-for-nixos.nix)
```

### Flakes

When using flakes, you could use something like this:

```
{
  description = "nix-dependencies-report demo";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nix-dependencies-report = {
      url = "git+https://codeberg.org/raboof/nix-dependencies-report";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, flake-utils, nix-dependencies-report }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
      in
      {
        packages = {
          dependency-report = (pkgs.callPackage nix-dependencies-report {}) pkgs.hello;
        };
      }
    );
}
```

Then `nix build .#dependency-report` will build the report for `pkgs.hello` and put it into `./result`.

## Limitations

* AFAIK Nix cannot distinguish between compile-time and runtime dependencies without actually building(/downloading) the derivation.
* If a derivation copies resources from a compile-time dependency into itself, the run-time closure obviously does 'contain' those resources, but the dependency will not be listed as a run-time dependency.

If you encounter more, please PR!

## Alternatives

* [sbomnix](https://github.com/tiiuae/sbomnix)
* [bombon](https://github.com/nikstur/bombon)

If you know of more, please PR!

## Credits

Heavily based on https://www.nmattia.com/posts/2019-10-08-runtime-dependencies.html
and https://gist.github.com/nmattia/93573354da7b24ba037145d81cb34bea
